﻿using System;
using System.Collections.Generic;
using System.Linq;
using fyp_chs.Services;
using fyp_chs.Models;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Service.Controllers
{
    public class PatientController : ApiController
    {
         private PatientRepository patientRepository;

    public PatientController()
    {
        this.patientRepository = new PatientRepository();
    }
    public Patient[] Get()
    {
        return patientRepository.GetAll();
    }
    }
}
