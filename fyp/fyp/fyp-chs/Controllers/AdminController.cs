﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ADMINSIDE.Controllers
{
    public class AdminController : Controller
    {
        //
        // GET: /Admin/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult AddDept()
        {
            return View();
        }
        public ActionResult AddDoc()
        {
            return View();
        }
        public ActionResult AddPatient()
        {
            return View();
        }
        public ActionResult login()
        {
            return View();
        }
        public ActionResult ViewDept()
        {
            return View();
        }
        public ActionResult ViewDoc()
        {
            return View();
        }
        public ActionResult ViewPat()
        {
            return View();
        }

    }
}
