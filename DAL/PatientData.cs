﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class PatientData
    {
        PatientEntities  data;
        public PatientData()
        {
            data = new PatientEntities();
        }
        public void setPatient(int id, string name)
        {
            Patient p = data.Patients.Find(id);
            if (p != null)
            {
                data.Patients.Find(id).PName = name;
                data.SaveChanges();

            }
        }
        public Patient getPatient(int id)
        {
            Patient p = data.Patients.Find(id);
            if (p != null)
            {
                return p;

            }
            else
                return null;
        }
        public List< Patient> getAll()
        {
            return data.Patients.ToList();
        }
    }
}
