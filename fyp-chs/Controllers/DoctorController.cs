﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using DAL;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.IO;

namespace fyp_chs.Controllers
{
    public class DoctorController : Controller
    {
        //
        // GET: /Doctor/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult AddPatient()
        {
            return View();
        }

        public ActionResult ViewAllPatients()
        {
            var clientside = new WebClient();


            try
            {
                var json = clientside.DownloadString("http://localhost:57168/api/Patient/Get");
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<Patient> pat = serializer.Deserialize<List<Patient>>(json);


                ViewBag.list = pat;
            }
            catch (WebException wex)
            {
                var pageContent = new StreamReader(wex.Response.GetResponseStream())
                        .ReadToEnd();
            }

            
            return View();
        }

        public ActionResult ManageAccount()
        {
            return View();
        }

        public ActionResult UpdateSchedule()
        {
            return View();
        }

        public ActionResult AppointmentRequests()
        {
            return View();
        }
        public ActionResult Calendar()
        {
            return View();
        }

        
    }
}
